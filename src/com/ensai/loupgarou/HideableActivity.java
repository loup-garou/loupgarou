package com.ensai.loupgarou;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

/**
 * Created by gpyh on 5/19/15.
 */
public abstract class HideableActivity extends Activity {

    private final static String TAG = "HideableActivity";

    private boolean hideOnPause = true;
    private boolean hidden = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hidden = false;
        Intent intent = getIntent();
        hideOnPause=intent.getBooleanExtra("must_hide",true);
    }


    // TODO override onCreateThumbnail to prevent cheating from Thumbnails

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause");
        if(hideOnPause) {
            findViewById(android.R.id.content).setVisibility(android.view.View.GONE);
            hidden = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if(hidden) {
            if(preferences.getBoolean("hide_activities",true)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_popup_gm)
                        .setMessage(R.string.description_popup_gm)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { //TODO button
                            public void onClick(DialogInterface dialog, int which) {
                                findViewById(android.R.id.content).setVisibility(View.VISIBLE);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } else {
                findViewById(android.R.id.content).setVisibility(View.VISIBLE);
            }
        }
		Editor editor = preferences.edit();
		editor.putBoolean("hide_activities", true);
		editor.commit();
    }
}
