package com.ensai.loupgarou;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GiveToGMActivity extends Activity implements OnClickListener {

	Button bGive = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_give_to_gm);
		
		bGive = (Button) findViewById(R.id.B_give_to_gm);
		bGive.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		finish();
	}
}
