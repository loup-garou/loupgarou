package com.ensai.loupgarou.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by gpyh on 5/17/15.
 */
public class GameAssociator {

    private List<RoleDescriptor> roleDescriptors;
	private Set<String> names;
    private List<Player> players;

    public GameAssociator(List<RoleDescriptor> roleDescriptors){
        this.roleDescriptors=roleDescriptors;
		this.names = new HashSet<String>();
		this.players = new ArrayList<Player>();
    }

//	public List<Player> getPlayers() {
//		return players;
//	}

//	public void addPlayer(Player player) {
//		players.add(player);
//	}

	// returns the associated roleDescriptor
	public RoleDescriptor randomAssociation(String namePlayer) throws DuplicatePlayerName {
		if(names.contains(namePlayer)){
			throw new DuplicatePlayerName();
		} else {
			names.add(namePlayer);
			RoleDescriptor associatedRole = roleDescriptors.remove(0);
			players.add(new Player(associatedRole.instantiateRole(), namePlayer));
			return associatedRole;
		}
	}

	public int getNumberNonRegisteredPlayers(){
		return roleDescriptors.size();
	}

	public List<Player> getPlayers() {
		return players;
	}

}
