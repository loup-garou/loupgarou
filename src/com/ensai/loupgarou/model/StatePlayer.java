package com.ensai.loupgarou.model;

public enum StatePlayer {
	NONEXISTENT,
	ALIVE{
		@Override
		public StatePlayer switchDeadOrAlive(){
			return DEAD;
		}
	},
	DEAD{
		@Override
		public StatePlayer switchDeadOrAlive(){
			return ALIVE;
		}
	};

	public StatePlayer switchDeadOrAlive(){
		return NONEXISTENT;
	}
}
