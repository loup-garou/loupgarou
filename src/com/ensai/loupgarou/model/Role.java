package com.ensai.loupgarou.model;

public class Role {

	private RoleDescriptor roleDescriptor;

	public Role(RoleDescriptor roleDescriptor) {
		super();
		this.roleDescriptor = roleDescriptor;
	}

	public RoleDescriptor getRoleDescriptor() {
		return roleDescriptor;
	}

//	public static List<Role> defaultRoles(Context contex){
//		List<Role> roles = new ArrayList<Role>();
//		String[] nameRoles = contex.getResources().getStringArray(R.array.name_roles);
//		String[] descriptionRoles = contex.getResources().getStringArray(R.array.description_roles);
//		for (int i = 0; i < Math.min(nameRoles.length,descriptionRoles.length); i++) {
//			roles.add(new Role(nameRoles[i], descriptionRoles[i]));
//		}
//		return roles;
//	}
//
//	public static Role searchARole(String name, Context context){
//		Role role = null;
//		for (Role r : defaultRoles(context)) {
//			if (name.equals(r.getName())) {
//				role=r;
//			}
//		}
//		return role;
//	}

	@Override
	public String toString() {
		return "Role [descriptor=" + roleDescriptor.toString() +"]";
	}

}
