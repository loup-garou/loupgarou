package com.ensai.loupgarou.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gpyh on 5/17/15.
 */
public class Game {

    private List<Player> players;
    private String villageName;

    public Game(List<Player> players, String villageName){
        this.players = players;
        this.villageName = villageName;
    }

    public List<Player> getPlayers(){
        return players;
    }

    public int getNumberPlayers(){
        return players.size();
    }

	public String getVillageName() {
		return villageName;
	}
}
