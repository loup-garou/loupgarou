package com.ensai.loupgarou.model;

import android.app.Application;

/**
 * Created by gpyh on 5/18/15.
 */
public class App extends Application{

    // Singleton class that manages the different states of the application
    // and allow activities to share data through the model

    private static App instance;

    private GameConfigurator gameConfigurator;
    private GameAssociator gameAssociator;
    private Game game;

    public App(){
        super();
        gameConfigurator=null;
        gameAssociator=null;
        game=null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
    }

    public void startConfiguration(){
        if(this.gameConfigurator==null) {
            this.gameConfigurator = new GameConfigurator();
        }
    }

    public void startAssociation(){
        if(this.gameAssociator==null) {
            this.gameAssociator = new GameAssociator(this.gameConfigurator.getRoles());
        }
    }

    public void startGame(){
        if(this.game==null){
            this.game=new Game(Player.fromNonExistentToAlive(this.gameAssociator.getPlayers()), 
            		gameConfigurator.getVillageName());
        }
    }

    public void wipeData(){
        instance = new App();
    }

    public GameConfigurator getGameConfigurator(){
        return gameConfigurator;
    }

    public GameAssociator getGameAssociator(){
        return gameAssociator;
    }

    public Game getGame(){
        return game;
    }

    public static App getInstance(){
        return instance;
    }
}
