package com.ensai.loupgarou.model;

import java.util.List;

public class Player {

	public Role role;
	public String name; 
	public StatePlayer state; 
	
	public Player(Role role, String name) {
		super();
		this.role = role;
		this.name = name;
		state = StatePlayer.NONEXISTENT;
	}

	public boolean toggleDeath(){
		state=state.switchDeadOrAlive();
		return (state==StatePlayer.DEAD);
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public String getRoleName(){
		return role.getRoleDescriptor().getName();
	}

	public static List<Player> fromNonExistentToAlive(List<Player> players) throws ExistentPlayerInNotStartedGame {
		for(Player player : players){
			if(player.state!=StatePlayer.NONEXISTENT) {
				throw new ExistentPlayerInNotStartedGame();
			}
			player.state=StatePlayer.ALIVE;
		}
		return players;
	}

	private static class ExistentPlayerInNotStartedGame extends RuntimeException{

	}
	
	
	
}
