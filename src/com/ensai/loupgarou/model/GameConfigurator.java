package com.ensai.loupgarou.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gpyh on 5/17/15.
 */
public class GameConfigurator {

    private Map<RoleDescriptor,Integer> roleQuantities;
    private int numberPlayers;
    private String villageName;

    public GameConfigurator(){
        roleQuantities = new HashMap<RoleDescriptor,Integer>();
        for(RoleDescriptor roleDescriptor : Arrays.asList(RoleDescriptor.values())){
            roleQuantities.put(roleDescriptor,0);
        }
        numberPlayers=0;
        villageName="";
    }

    // returns the modified value

    public int decrementRoleDescriptor(RoleDescriptor roleDescriptor){
        int value = roleQuantities.get(roleDescriptor);
        if(value>0){
            numberPlayers--;
            roleQuantities.put(roleDescriptor,value-1);
            return value-1;
        } else {
            return 0;
        }
    }

    public int incrementRoleDescriptor(RoleDescriptor roleDescriptor){
        int value = roleQuantities.get(roleDescriptor);
        numberPlayers++;
        roleQuantities.put(roleDescriptor,value+1);
        return value+1;
    }

    public List<RoleDescriptor> getRoles(){
        List<RoleDescriptor> roles = new ArrayList<RoleDescriptor>();
        for(RoleDescriptor roleDescriptor : roleQuantities.keySet()){
            for (int i = 0; i < roleQuantities.get(roleDescriptor); i++) {
                roles.add(roleDescriptor);
            }
        }
        Collections.shuffle(roles);
        return roles;
    }

    public int getNumberPlayers(){
        return numberPlayers;
    }

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
    
    

}
