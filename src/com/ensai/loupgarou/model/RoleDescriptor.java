package com.ensai.loupgarou.model;

import com.ensai.loupgarou.R;

/**
 * Created by gpyh on 5/18/15.
 */
public enum RoleDescriptor {
    WEREWOLF,
    TOWNFOLK,
    SEER,
    WITCH,
    HUNTER,
    CUPID,
    LITTLE_GIRL;

    public Role instantiateRole(){
        return new Role(this);
    }

    public String getName(){
        return(App.getInstance().getResources().getStringArray(R.array.name_roles)[this.ordinal()]);
    }

    public String getDescription(){
        return(App.getInstance().getResources().getStringArray(R.array.description_roles)[this.ordinal()]);
    }

    public static RoleDescriptor get(int i){
        return values()[i];
    }

}