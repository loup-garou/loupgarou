package com.ensai.loupgarou;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.ensai.loupgarou.model.App;
import com.ensai.loupgarou.model.RoleDescriptor;

/**
 * Created by gpyh on 5/19/15.
 */
public class RoleOnClickListener implements View.OnClickListener{

    private boolean mustHide;
    private RoleDescriptor roleDescriptor;
    private Activity activity;

    public RoleOnClickListener(Activity activity, RoleDescriptor roleDescriptor, boolean mustHide) {
        this.activity = activity;
        this.roleDescriptor=roleDescriptor;
        this.mustHide=mustHide;
    }

    public RoleOnClickListener(Activity activity, RoleDescriptor roleDescriptor) {
        this.activity = activity;
        this.roleDescriptor=roleDescriptor;
        this.mustHide=true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(App.getInstance(), RoleActivity.class);
        intent.putExtra("pos_role", roleDescriptor.ordinal());
        intent.putExtra("must_hide",mustHide);
        activity.startActivity(intent);
    }
}
