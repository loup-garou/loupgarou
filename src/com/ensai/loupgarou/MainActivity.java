package com.ensai.loupgarou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	// Tester l'appli en apk, essayer de la faire tourner sur le plus de configurations possibles.
	
	private static final String TAG = "MainActivity";
	Button bBegin = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.i(TAG,"Initialisation de Main Activity");
		
		bBegin = (Button) findViewById(R.id.B_begin);
		bBegin.setOnClickListener(this);
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.B_begin:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
	}
}
