package com.ensai.loupgarou;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ensai.loupgarou.model.App;
import com.ensai.loupgarou.model.Role;
import com.ensai.loupgarou.model.RoleDescriptor;

public class SettingsActivity extends Activity implements OnClickListener {

	private static final String TAG = "SettingsActivity";

	ArrayList<Role> roles;
	ArrayList<Integer> number = new ArrayList<Integer>();
	
	ListView lVRoles = null;
	LinearLayout[] lLRole;
	EditText eTGameName = null;
	TextView tVMinus = null;
	TextView tVPlus = null;
	TextView tVRole = null;
	Button bSave = null;
	CustomRoleAdapter adapter;

//	public void addOneToIThNumber(int i){
//		number.set(i, number.get(i)+1);
//	}
//
//	public void removeOneToIThNumber(int i){
//		number.set(i, number.get(i)-1);
//	}

	public class CustomRoleAdapter extends BaseAdapter{
		private static final String TAG = "CustomRoleAdapter";

		public CustomRoleAdapter() {
			super();
		}

		// How many items are in the data set represented by this Adapter.
		@Override
		public int getCount() {
			//Log.i(TAG,"*** 2 getCount method");
			//Log.i(TAG, Integer.toString(innerClassRoleArray.size()));
			return RoleDescriptor.values().length;
		}

		// Get the data item associated with the specified position in the data set.
		@Override
		public Object getItem(int position) {
			//Log.i(TAG,"*** ? getItem method");
			//Log.i(TAG,innerClassRoleArray.get(position).toString());
			return RoleDescriptor.values()[position];
		}

		// Get the row id associated with the specified position in the list.
		@Override
		public long getItemId(int position) {
			//Log.i(TAG,"*** 3 getItemId method");
			//Log.i(TAG,Integer.toString(innerClassRoleArray.get(position).getId()));
			return position;
		}

		// Get a View that displays the data at the specified position in the data set.
		// You can either create a View manually or inflate it from an XML layout file.
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			Log.i(TAG,"*** 4 getView method");
			Log.i(TAG,Integer.toString(position));

			if(convertView == null){
				// LayoutInflater class is used to instantiate layout XML file into its corresponding View objects.
				LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = layoutInflater.inflate(R.layout.layout_role, null);

				lLRole[position] = (LinearLayout) convertView.findViewById(R.id.LL_role);
				((TextView) lLRole[position].getChildAt(0)).setText(RoleDescriptor.get(position).getName());
				((TextView) lLRole[position].getChildAt(1)).setText("0");
			} else {
				lLRole[position] = (LinearLayout) convertView.findViewById(R.id.LL_role);
			}


			//Log.i(TAG,"Nb children lLRole : "+Integer.toString(lLRole.getChildCount()));


			//rawTextView.setText(innerClassRoleArray.get(position).getName());

			lLRole[position].getChildAt(0).setOnClickListener(new RoleOnClickListener(SettingsActivity.this,RoleDescriptor.get(position),false));


			lLRole[position].getChildAt(2).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView tVNumber = (TextView) ((LinearLayout) v.getParent()).getChildAt(1);
					Log.v(TAG, "trying to decrement" + RoleDescriptor.get(position).toString());
					tVNumber.setText(Integer.toString(
							App.getInstance().getGameConfigurator().decrementRoleDescriptor(RoleDescriptor.get(position))
					));
				}
			});

			lLRole[position].getChildAt(3).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView tVNumber = (TextView) ((LinearLayout) v.getParent()).getChildAt(1);
					int number = Integer.parseInt((String) tVNumber.getText());
					Log.v(TAG, "incrementing " + RoleDescriptor.get(position).toString());
					tVNumber.setText(Integer.toString(
							App.getInstance().getGameConfigurator().incrementRoleDescriptor(RoleDescriptor.get(position))
					));
				}
			});
			
			return convertView;
		}
		
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		bSave = (Button) findViewById(R.id.B_save_config);
		eTGameName = (EditText) findViewById(R.id.ET_game_name);
		lVRoles = (ListView) findViewById(R.id.LV_roles);

		App.getInstance().startConfiguration();

		lLRole = new LinearLayout[RoleDescriptor.values().length];

		adapter = new CustomRoleAdapter();
		lVRoles.setAdapter(adapter);
		bSave.setOnClickListener(this);
	}
	// Pour int�grer des choses au ListView, il faut �tendre BaseAdapter (plut�t que ArrayAdapter) et faire un adapter



	@Override
	public void onClick(View v) {
		//Log.i(TAG,"On rentre dans le onClick de SettingsActivity");
		switch (v.getId()) {
		case R.id.B_save_config:
			// Checking for errors
			if (eTGameName.getText().toString().length() == 0) {
				Toast.makeText(this, R.string.errNoGameName, Toast.LENGTH_LONG).show();
			} else if (App.getInstance().getGameConfigurator().getNumberPlayers() == 0) {
				Toast.makeText(this, R.string.errNoRole, Toast.LENGTH_LONG).show();
			} else {
				// No error
				// Confirmation dialog
				new AlertDialog.Builder(this)
			    .setTitle(R.string.begin)
			    .setMessage(R.string.ready)
			    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // yes
			        	App.getInstance().getGameConfigurator().setVillageName(eTGameName.getText().toString());
						Intent intent = new Intent(getApplicationContext(), AssociatonActivity.class);
						startActivity(intent);
						finish();
			        }
			     })
			    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing
			        }
			     })
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
			}
			break;
		default:
			break;
		}
	}
	
}
