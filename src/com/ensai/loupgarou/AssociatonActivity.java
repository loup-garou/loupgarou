package com.ensai.loupgarou;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ensai.loupgarou.model.App;
import com.ensai.loupgarou.model.DuplicatePlayerName;
import com.ensai.loupgarou.model.RoleDescriptor;

public class AssociatonActivity extends HideableActivity implements OnClickListener {

	private final String TAG = "Attribution";

	// k est d�fini tel que le k�me joueur est en train de rentrer son nom et d�couvre son r�le.
	int k = 1;
	
	Button bWhichRole = null;
	TextView tVYouAre = null;
	TextView tVRole = null;
	TextView tVNextPlayer = null;
	Button bNextPlayer = null;
	EditText eTName = null;

	private void initiateAsking(){
		bWhichRole.setVisibility(android.view.View.VISIBLE);
		tVYouAre.setText(R.string.you_are);
		tVRole.setText(R.string.NA);
		tVRole.setVisibility(android.view.View.GONE);
		tVNextPlayer.setVisibility(android.view.View.GONE);
		bNextPlayer.setVisibility(android.view.View.GONE);
		eTName.setText("");
		Log.v(TAG, "Number of non registered players" + App.getInstance().getGameAssociator().getNumberNonRegisteredPlayers());
		if(App.getInstance().getGameAssociator().getNumberNonRegisteredPlayers()==1){
			tVNextPlayer.setText("");
			bNextPlayer.setText(R.string.give_to_gm);
		};
		eTName.setEnabled(true);
	}

	private void answerAssociation(){
		bWhichRole.setVisibility(android.view.View.GONE);
		tVYouAre.setText(R.string.you_are2);
		tVRole.setVisibility(android.view.View.VISIBLE);
		tVNextPlayer.setVisibility(android.view.View.VISIBLE);
		bNextPlayer.setVisibility(android.view.View.VISIBLE);
		eTName.setEnabled(false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attribution);

		bWhichRole = (Button) findViewById(R.id.B_which_role);
		bNextPlayer = (Button) findViewById(R.id.B_next_player);
		tVYouAre = (TextView) findViewById(R.id.TV_you_are);
		tVRole = (TextView) findViewById(R.id.TV_role);
		tVNextPlayer = (TextView) findViewById(R.id.TV_next_player);
		eTName = (EditText) findViewById(R.id.ET_name);

		bWhichRole.setOnClickListener(this);
		bNextPlayer.setOnClickListener(this);

		App.getInstance().startAssociation();

		initiateAsking();
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.B_which_role:
			try {
				if (eTName.getText().toString().length()==0) {
					Toast.makeText(this, R.string.errEmptyPlayerName, Toast.LENGTH_LONG).show();
				} else {
					RoleDescriptor currentRoleDescriptor=App.getInstance().getGameAssociator().randomAssociation(eTName.getText().toString());
					tVRole.setText(currentRoleDescriptor.getName());
					tVRole.setOnClickListener(new RoleOnClickListener(this,currentRoleDescriptor));
					answerAssociation();
				}
			} catch (DuplicatePlayerName duplicatePlayerName) {
				duplicatePlayerName.printStackTrace();
				Toast.makeText(this, R.string.errDuplicatePlayerName, Toast.LENGTH_LONG).show();
			}

//			App.getInstance().getGameAssociator().addPlayer(
//					new Player(App.getInstance().getGameAssociator().getRoleDescriptors().get(k),
//							eTName.getText().toString()));
			break;
		case R.id.B_next_player:
			if(App.getInstance().getGameAssociator().getNumberNonRegisteredPlayers()==0){
				Intent intent = new Intent(this, GameActivity.class);
				startActivity(intent);
				finish();
			} else {
				initiateAsking();
			}
			//TODO partie m�tier + changement de vues
			break;
		default:
			break;
		}
	}

}
