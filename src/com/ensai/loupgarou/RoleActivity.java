package com.ensai.loupgarou;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.ensai.loupgarou.model.RoleDescriptor;

public class RoleActivity extends HideableActivity {

	RoleDescriptor roleDescriptor = null;
	
	private static final String TAG = "RoleActivity";
	TextView tVRole = null;
	TextView tVDescription = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_role);
		//Log.i(TAG,"Initialisation de Role Activity");
		Intent intent = getIntent();
		roleDescriptor = RoleDescriptor.get(intent.getIntExtra("pos_role",-1));
		

		tVRole = (TextView) findViewById(R.id.TV_role);
		tVDescription = (TextView) findViewById(R.id.TV_description);
		
		tVRole.setText(roleDescriptor.getName());
		tVDescription.setText(roleDescriptor.getDescription());
		
		//Log.i(TAG,"Fin d'initialisation de Role Activity");
	}

	@Override
	protected void onStop() {
		super.onStop();
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor = preferences.edit();
		editor.putBoolean("hide_activites", false);
		editor.commit();
	}
	

}