package com.ensai.loupgarou;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ensai.loupgarou.model.App;
import com.ensai.loupgarou.model.Player;
import com.ensai.loupgarou.model.RoleDescriptor;

public class GameActivity extends HideableActivity implements OnClickListener{

	private final String TAG="GameActivity";

	RelativeLayout rLGame = null;
	ListView lVRoles = null;
	Button bEnd = null;
	LinearLayout[] lLPlayer;
	CustomPlayerAdapter adapter;


	public class CustomPlayerAdapter extends BaseAdapter{
		private static final String TAG = "CustomPlayerAdapter";
		int currentlyFocusedRow;

		public CustomPlayerAdapter() {
			super();
		}

		// How many items are in the data set represented by this Adapter.
		@Override
		public int getCount() {
			//Log.i(TAG,"*** 2 getCount method");
			//Log.i(TAG, Integer.toString(innerClassRoleArray.size()));
			return App.getInstance().getGame().getNumberPlayers();
		}

		// Get the data item associated with the specified position in the data set.
		@Override
		public Object getItem(int position) {
			//Log.i(TAG,"*** ? getItem method");
			//Log.i(TAG,innerClassRoleArray.get(position).toString());
			return App.getInstance().getGame().getPlayers().get(position);
		}

		private Player getPlayer(int position) {
			return App.getInstance().getGame().getPlayers().get(position);
		}

		// Get the row id associated with the specified position in the list.
		@Override
		public long getItemId(int position) {
			//Log.i(TAG,"*** 3 getItemId method");
			//Log.i(TAG,Integer.toString(innerClassRoleArray.get(position).getId()));
			return position;
		}

		// Get a View that displays the data at the specified position in the data set.
		// You can either create a View manually or inflate it from an XML layout file.
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			Log.i(TAG,"*** 4 getView method");
			Log.i(TAG, Integer.toString(position));

			if(convertView == null){
				// LayoutInflater class is used to instantiate layout XML file into its corresponding View objects.
				LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = layoutInflater.inflate(R.layout.layout_player, null);
			}

			lLPlayer[position] = (LinearLayout) convertView.findViewById(R.id.LL_player);

			//Log.i(TAG,"Nb children lLPlayer : "+Integer.toString(lLPlayer.getChildCount()));

			((TextView) ((LinearLayout) lLPlayer[position].getChildAt(0)).getChildAt(0)).setText(
					getPlayer(position).getName()
			);
			((TextView) ((LinearLayout) lLPlayer[position].getChildAt(0)).getChildAt(1)).setText(
					getPlayer(position).getRoleName()
			);

			/**
			 *

			 It is indeed happening because all the views are redrawn, so the edit text representing whatever
			 row used to be focused is now a completely different object. Set a variable in your adapter: int currentlyFocusedRow;

			 in getView for your adapter: Add an onFocusChanged listener to each edit text and when that edit
			 text gains focus, set currentlyFocusedRow = whatever row the focused edit text happens to be in.
			 Also set any edit text that is in the currentlyFocusedRow to be focused.

			 */
			//rawTextView.setText(innerClassRoleArray.get(position).getName());


			((EditText) lLPlayer[position].getChildAt(1)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View view, boolean b) {
					if (b){
						currentlyFocusedRow = position;
					}
				}
			});
			((EditText) lLPlayer[position].getChildAt(1)).requestFocus();


			((LinearLayout) lLPlayer[position].getChildAt(0)).getChildAt(1).setOnClickListener(new RoleOnClickListener(GameActivity.this, RoleDescriptor.get(position)));


			((LinearLayout) lLPlayer[position].getChildAt(0)).getChildAt(2).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView tVDeadOrAlive = (TextView) v;
					boolean isDead = getPlayer(position).toggleDeath();

					if(isDead){
						tVDeadOrAlive.setText(getResources().getString(R.string.dead));
						tVDeadOrAlive.setBackgroundColor(getResources().getColor(R.color.red));
					} else {
						tVDeadOrAlive.setText(getResources().getString(R.string.alive));
						tVDeadOrAlive.setBackgroundColor(getResources().getColor(R.color.green));
					}
				}
			});

			((LinearLayout) lLPlayer[position].getChildAt(0)).getChildAt(3).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					EditText eTNotes = (EditText) ((LinearLayout) ((TextView) v).getParent().getParent()).getChildAt(1);
					if (eTNotes.getVisibility() == android.view.View.GONE) {
						eTNotes.setVisibility(android.view.View.VISIBLE);
					} else {
						eTNotes.setVisibility(android.view.View.GONE);
					}
				}
			});

			return convertView;
		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		App.getInstance().startGame();

		setContentView(R.layout.activity_game);

		rLGame = (RelativeLayout) findViewById(R.id.RL_game);

		bEnd = (Button) findViewById(R.id.B_end_game);
		lVRoles = (ListView) findViewById(R.id.LV_player);

		if (App.getInstance().getGame().getVillageName().length() > 0) {
			setTitle(getString(R.string.title_activity_game2)+" "+App.getInstance().getGame().getVillageName());
		}
		
		lLPlayer = new LinearLayout[App.getInstance().getGame().getNumberPlayers()];

		adapter = new CustomPlayerAdapter();
		Log.v(TAG,adapter.toString());
		lVRoles.setAdapter(adapter);
		bEnd.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.B_end_game:
			new AlertDialog.Builder(this)
			.setTitle(R.string.title_popup_end_game)
			.setMessage(R.string.description_popup_end_game)
			.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					// yes
					Intent intent = new Intent(getApplicationContext(), MainActivity.class);
					startActivity(intent);
					finish();
					moveTaskToBack(true);
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			})
			.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					// do nothing
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
			break;
		default:
			break;
		}
	}
}
